// implement fork from user space

#include <inc/string.h>
#include <inc/lib.h>

// PTE_COW marks copy-on-write page table entries.
// It is one of the bits explicitly allocated to user processes (PTE_AVAIL).
#define PTE_COW		0x800

//
// Custom page fault handler - if faulting page is copy-on-write,
// map in our own private writable copy.
//
static void
pgfault(struct UTrapframe *utf)
{
	void *addr = (void *) utf->utf_fault_va;
	uint32_t err = utf->utf_err;
	int r;

	// Check that the faulting access was (1) a write, and (2) to a
	// copy-on-write page.  If not, panic.
	// Hint:
	//   Use the read-only page table mappings at uvpt
	//   (see <inc/memlayout.h>).

	// LAB 4: Your code here.
    if ((err & FEC_WR)
            && (PTE_COW | PTE_P) == (uvpt[PGNUM(addr)] & (PTE_COW | PTE_P))) {
        r = sys_page_alloc(0, (void*)PFTEMP, PTE_P | PTE_W | PTE_U);
        if (r) {
            panic("sys_page_alloc() failed: %d\n", r);
        }
        memcpy((void*)PFTEMP, ROUNDDOWN(addr, PGSIZE), PGSIZE);
        r = sys_page_map(0, (void*)PFTEMP, 0, ROUNDDOWN(addr, PGSIZE),
                PTE_P | PTE_W | PTE_U);
        if (r) {
            panic("sys_page_map() failed: %d\n", r);
        }
        r = sys_page_unmap(0, (void*)PFTEMP);
        if (r) {
            panic("sys_page_unmap() failed: %d\n", r);
        }
        return;
    } else {
        panic("unexpected page fault at addr %p: %e", addr, err);
    }

	// Allocate a new page, map it at a temporary location (PFTEMP),
	// copy the data from the old page to the new page, then move the new
	// page to the old page's address.
	// Hint:
	//   You should make three system calls.
	//   No need to explicitly delete the old page's mapping.

	// LAB 4: Your code here.

	panic("pgfault not implemented");
}

//
// Map our virtual page pn (address pn*PGSIZE) into the target envid
// at the same virtual address.  If the page is writable or copy-on-write,
// the new mapping must be created copy-on-write, and then our mapping must be
// marked copy-on-write as well.  (Exercise: Why do we need to mark ours
// copy-on-write again if it was already copy-on-write at the beginning of
// this function?)
//
// Returns: 0 on success, < 0 on error.
// It is also OK to panic on error.
//
static int
duppage(envid_t envid, unsigned pn)
{
	int r;

	// LAB 4: Your code here.
	// panic("duppage not implemented");
	// return 0;

    // LAB 5
    if (uvpt[pn] & PTE_SHARE) {
        r = sys_page_map(0, (void*)(pn*PGSIZE),
                envid, (void*)(pn*PGSIZE),
                uvpt[pn] & PTE_SYSCALL);
        return r;
    }
    // I don't know why our mapping also needs to be marked COW again.
    int perm = PTE_P | PTE_U | PTE_COW;
    r = sys_page_map(0, (void*)(pn*PGSIZE), envid, (void*)(pn*PGSIZE), perm);
    if (r)
        panic("duppage() failed: %e\n", r);
    r = sys_page_map(envid, (void*)(pn*PGSIZE), 0, (void*)(pn*PGSIZE), perm);
    if (r)
        panic("duppage() failed: %e\n", r);

    return r;
}

//
// User-level fork with copy-on-write.
// Set up our page fault handler appropriately.
// Create a child.
// Copy our address space and page fault handler setup to the child.
// Then mark the child as runnable and return.
//
// Returns: child's envid to the parent, 0 to the child, < 0 on error.
// It is also OK to panic on error.
//
// Hint:
//   Use uvpd, uvpt, and duppage.
//   Remember to fix "thisenv" in the child process.
//   Neither user exception stack should ever be marked copy-on-write,
//   so you must allocate a new page for the child's user exception stack.
//
envid_t
fork(void)
{
	// LAB 4: Your code here.
	// panic("fork not implemented");

    // will also setup exception stack
    set_pgfault_handler(pgfault);

    envid_t child_id = sys_exofork();
    if (child_id < 0) {
        return child_id;
    } else if (child_id == 0) {
        thisenv = &envs[ENVX(sys_getenvid())];
        return 0;
    }

    int err;

    unsigned pgnum;
    for (pgnum = 0; pgnum * PGSIZE < UTOP; pgnum++) {
        if (pgnum * PGSIZE == UXSTACKTOP - PGSIZE) {
            continue;
        }
        pde_t pde = uvpd[PDX(pgnum * PGSIZE)];

        // PTE_COW is only used by us; it's not defined in kernel
        // also, I think user part page directory entries always have
        // (PTE_W | PTE_U) set if they are valid?
        if ((pde & PTE_P)/* && (pde & PTE_U) && (pde & PTE_W)*/) {
            pte_t pte = uvpt[pgnum];
            if ((pte & (PTE_P | PTE_U)) != (PTE_P | PTE_U)) {
                continue;
            }
            if ((pte & PTE_W) || (pte & PTE_COW)) {
                err = duppage(child_id, pgnum);
            } else {
                err = sys_page_map(
                        0, (void*)(pgnum*PGSIZE),
                        child_id, (void*)(pgnum*PGSIZE),
                        PTE_P | PTE_U);
            }

            if (err) {
                panic("fork() failed: %e\n", err);
            }
        } else {
            pgnum += NPTENTRIES - 1;
        }
    }

    err = sys_page_alloc(child_id, (void *)(UXSTACKTOP - PGSIZE),
            PTE_P | PTE_U | PTE_W);
    if (err) {panic("fork() failed: %e\n", err);}
    err = sys_env_set_pgfault_upcall(child_id, thisenv->env_pgfault_upcall);
    if (err) {panic("fork() failed: %e\n", err);}

    err = sys_env_set_status(child_id, ENV_RUNNABLE);
    if (err) {
        panic("fork() failed: %e\n", err);
    }
    return child_id;
}

// Challenge!
int
sfork(void)
{
	panic("sfork not implemented");
	return -E_INVAL;
}
